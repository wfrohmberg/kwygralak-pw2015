﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Drawing;

namespace TuringMachine
{
    public abstract class TuringAbstract
    {
        public abstract void Step();
        public abstract void redraw(System.Drawing.Graphics panel1draw, System.Drawing.Pen pen);
        public abstract void dispalyTransition(System.Windows.Forms.RichTextBox richTextBox1);
    }

    public class TuringMachine<AlphabetType, StateType> :TuringAbstract, IEnumerable<AlphabetType>
    {
        public StateType actualState;
        public List<AlphabetType> Tape = new List<AlphabetType>();
        public List<StateType> States = new List<StateType>();
        public List<Transition> Transitions = new List<Transition>();
        public int Carret;
        public int transToDisp;

        public TuringMachine()
        {
        }

        public TuringMachine(Settings1 sett)
        {
            actualState = (StateType)System.Convert.ChangeType(sett.StartState, typeof(StateType));
            Carret = (int)System.Convert.ChangeType(sett.Carret, typeof(int));
            foreach (Settings1.Transition x in sett.Transitions)
            {
                Transition temp = new Transition();
                temp.state = (StateType)System.Convert.ChangeType(x.state, typeof(StateType));
                temp.value = (AlphabetType)System.Convert.ChangeType(x.value, typeof(AlphabetType));
                temp.nextState = (StateType)System.Convert.ChangeType(x.nextState, typeof(StateType));
                temp.newValue = (AlphabetType)System.Convert.ChangeType(x.newValue, typeof(AlphabetType));
                temp.direction = (int)System.Convert.ChangeType(x.direction, typeof(int));
                Transitions.Add(temp);
            }

            foreach (string x in sett.Tape)
            {
                AlphabetType temp = (AlphabetType)System.Convert.ChangeType(x, typeof(AlphabetType));
                Tape.Add(temp);
            }
        }

        public override void Step()
        {
            transToDisp = 0;
            foreach (Transition i in Transitions)
            {
                //if (i.state.ToString().Equals(actualState.ToString()) && i.value.ToString().Equals(Tape.ElementAt(Carret).ToString()))
                if (i.state.Equals(actualState) && i.value.Equals(Tape.ElementAt(Carret)))
                {
                    Tape[Carret] = i.newValue;
                    actualState = i.nextState;
                    Carret += i.direction;
                    return;
                }
                transToDisp++;
            }
            //TODO: THROW EXCEPTION
            Console.WriteLine("Cant find transition!");
            throw new ArgumentOutOfRangeException();
            //return null;
        }

        //------------------------------------------DISPLAY ----------------------------------------------

        public override void redraw(System.Drawing.Graphics panel1draw, System.Drawing.Pen pen)
        {
            panel1draw.Clear(System.Drawing.Color.White);
            panel1draw.DrawRectangle(pen, new Rectangle(0, 0, 249, 249));
            
            int max = Tape.ElementAt(0).ToString().Length;
            foreach (AlphabetType x in Tape)
            {
                if (max > x.ToString().Length)
                {
                    max = x.ToString().Length;
                }
            }
            Size rectangleSize = new Size(40+(max-1)*10,40);
            int inLine = 0;
            int i = 20, j = 40;
            while (i < 200)
            {
                i += rectangleSize.Width;
                inLine++;
            }
            i = 20;
            foreach (AlphabetType x in Tape)
            {
                panel1draw.DrawRectangle(pen, new Rectangle(new Point(i, j), rectangleSize));

                panel1draw.DrawString(x.ToString(), new Font("Arial", 16), new SolidBrush(System.Drawing.Color.Black), new Point(i + 5, j + 10));
                i += rectangleSize.Width;
                if (i + rectangleSize.Width > 250)
                {
                    i = 20;
                    j += 80;
                }
            }

            panel1draw.DrawEllipse(pen, 30 + rectangleSize.Width * (Carret % inLine), 20 + 80 * (int)(System.Math.Floor((decimal)Carret / inLine)), 15, 15);
        }

        public override void dispalyTransition(System.Windows.Forms.RichTextBox richTextBox1)
        {
            //Console.WriteLine(Transitions.Count + "\t" + transToDisp);
            Transition temp = Transitions.ElementAt(transToDisp);
            richTextBox1.Clear();
            richTextBox1.AppendText("State:\t\t"+temp.state.ToString() + "\n" +
                                    "Value:\t\t" + temp.value.ToString() + "\n\n" +
                                    "nextState:\t" + temp.nextState.ToString() + "\n" +
                                    "newValue:\t" + temp.newValue.ToString() + "\n" +
                                    "Direction:\t\t" + temp.direction.ToString() + "\n");
        }

        //------------------------------------------TRANSITION CLASS ----------------------------------------------

        public class Transition
        {
            public StateType state;
            public AlphabetType value;

            public StateType nextState;
            public AlphabetType newValue;
            public int direction;

            public Transition()
            {
            }

            public Transition(Tuple<StateType, AlphabetType, StateType, AlphabetType, int> param1)
            {
                state = param1.Item1;
                value = param1.Item2;
                nextState = param1.Item3;
                newValue = param1.Item4;
                direction = param1.Item5;
                
            }
        }

        //------------------------------------------OPERATOR += ----------------------------------------------

        public static TuringMachine<AlphabetType, StateType> operator +(TuringMachine<AlphabetType, StateType> param1, StateType param2)
        {
            param1.States.Add(param2);
            return param1;
        }

        public static TuringMachine<AlphabetType, StateType> operator +(TuringMachine<AlphabetType, StateType> param1, Tuple<StateType, AlphabetType, StateType, AlphabetType, int> param2)
        {
            Transition temp = new Transition(param2);
            param1.Transitions.Add(temp);
            return param1;
        }

        //------------------------------------------GET/SET ----------------------------------------------
        public int getCarret()
        {
            return Carret;
        }
        
        public void setCarret(int i)
        {
            Carret = i;
        }

        //------------------------------------------ENUMERATOR----------------------------------------------
        private IEnumerator<AlphabetType> getItem()
        {
            foreach (AlphabetType c in Tape)
            {
                yield return c;
            }
        }

        public IEnumerator<AlphabetType> GetEnumerator()
        {
            return getItem();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return getItem();
        }

    }
}
