﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TuringMachine
{
    public class Settings1
    {
        public string AlphabetType;
        public string StateType;
        public string StartState;
        public string Carret;
        public List<string> Tape;
        public List<Transition> Transitions;
        public string assemblyPath;

        public Settings1()
        {
            Tape = new List<string>();
            Transitions = new List<Transition>();
            assemblyPath = "";
        }

        public class Transition
        {
            public string state;
            public string value;

            public string nextState;
            public string newValue;
            public string direction;


            public Transition() 
            { 
            }

            public Transition(Tuple<String, String, String, String, String> param1)
            {
                state = param1.Item1;
                value = param1.Item2;
                nextState = param1.Item3;
                newValue = param1.Item4;
                direction = param1.Item5;
            }
        }

        public static Settings1 operator +(Settings1 param1, Tuple<String, String, String, String, String> param2)
        {
            Transition temp = new Transition(param2);
            param1.Transitions.Add(temp);
            return param1;
        }

        public void duplicateSett(Settings1 param1)
        {
            Carret = param1.Carret;
            AlphabetType = param1.AlphabetType;
            StateType = param1.StateType;
            StartState = param1.StartState;
            assemblyPath = param1.assemblyPath;
            
            //Transitions = param1.Transitions;
            Transitions.Clear();
            foreach (Transition t in param1.Transitions)
            {
                //this += new Tuple<string, string, string, string, string>(t.state, t.value, t.nextState, t.nextState, t.direction);
                Transitions.Add(new Transition(new Tuple<string, string, string, string, string>(t.state, t.value, t.nextState, t.newValue, t.direction)));
            }

            //Tape = param1.Tape;
            Tape.Clear();
            foreach (string s in param1.Tape)
            {
                Tape.Add(s);
            }
        }

        public void editTrans(int x,string s)
        {
            string[] split = s.Split('\n');
            if (split.Count() >= 6)
            {
                Transitions.ElementAt(x).state = split[0];
                Transitions.ElementAt(x).value = split[1];
                Transitions.ElementAt(x).nextState = split[3];
                Transitions.ElementAt(x).newValue = split[4];
                Transitions.ElementAt(x).direction = split[5];
            }
            else
            {
                throw new ArgumentOutOfRangeException();
            }
        }

        public bool isSettValid()
        {
            if (AlphabetType == null || AlphabetType == "")
            {
                return false;
            }
            if (StateType == null || StateType == "")
            {
                return false;
            }
            if (Carret == null || Carret == "")
            {
                return false;
            }
            if (StartState == null || StartState == "")
            {
                return false;
            }
            if (Tape.Count < 1)
            {
                return false;
            }
            foreach (string s in Tape)
            {
                if (s == null || s == "")
                {
                    return false;
                }
            }

            if (Transitions.Count < 1)
            {
                return false;
            }
            foreach (Transition x in Transitions)
            {
                if (x.state == null || x.state == "")
                {
                    return false;
                }
                if (x.value == null || x.value == "")
                {
                    return false;
                }
                if (x.nextState == null || x.nextState == "")
                {
                    return false;
                }
                if (x.newValue == null || x.newValue == "")
                {
                    return false;
                }
                if (x.direction == null || x.direction == "")
                {
                    return false;
                }
            }
            return true;
        }
    }
}
