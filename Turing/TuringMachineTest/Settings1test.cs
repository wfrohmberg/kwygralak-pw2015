﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TuringMachine;

namespace TuringMachineTest
{
    [TestClass]
    public class Settings1test
    {
        public Settings1 testSett;

        public void initSett()
        {
            testSett = new Settings1();
            testSett.StartState = "0";
            testSett.Carret = "0";
            testSett.Tape.Add("a");
            testSett.Tape.Add("b");
            testSett += new Tuple<string, string, string, string, string>("0", "a", "1", "b", "1");
            testSett += new Tuple<string, string, string, string, string>("0", "b", "1", "a", "1");
            testSett += new Tuple<string, string, string, string, string>("1", "a", "1", "b", "-1");
            testSett += new Tuple<string, string, string, string, string>("1", "b", "0", "a", "-1");

            testSett.AlphabetType = "char";
            testSett.StateType = "int";
        }

        [TestMethod]
        public void Test_duplicateSett_1()
        {
            initSett();

            Settings1 tempSett = new Settings1();
            tempSett.StateType = "1";
            tempSett.AlphabetType = "2";
            tempSett.StartState = "3";
            tempSett.Carret = "4";
            tempSett.assemblyPath = "5";
            tempSett.Tape.Add("6");
            tempSett += new Tuple<string, string, string, string, string>("7", "8", "9", "10", "11");

            testSett.duplicateSett(tempSett);

            Assert.AreEqual(tempSett.StateType, testSett.StateType);
            Assert.AreEqual(tempSett.AlphabetType, testSett.AlphabetType);
            Assert.AreEqual(tempSett.StartState, testSett.StartState);
            Assert.AreEqual(tempSett.Carret, testSett.Carret);
            Assert.AreEqual(tempSett.assemblyPath, testSett.assemblyPath);

            Assert.AreEqual(tempSett.Tape[0], testSett.Tape[0]);
            Assert.AreEqual(tempSett.Transitions[0].state, testSett.Transitions[0].state);
            Assert.AreEqual(tempSett.Transitions[0].value, testSett.Transitions[0].value);
            Assert.AreEqual(tempSett.Transitions[0].nextState, testSett.Transitions[0].nextState);
            Assert.AreEqual(tempSett.Transitions[0].newValue, testSett.Transitions[0].newValue);
            Assert.AreEqual(tempSett.Transitions[0].direction, testSett.Transitions[0].direction);
            
        }

        [TestMethod]
        public void Test_editTrans_1()
        {
            initSett();
            
            string param2 = "1\n2\n\n3\n4\n5";
            testSett.editTrans(0, param2);

            Assert.AreEqual("1", testSett.Transitions[0].state);
            Assert.AreEqual("2", testSett.Transitions[0].value);
            Assert.AreEqual("3", testSett.Transitions[0].nextState);
            Assert.AreEqual("4", testSett.Transitions[0].newValue);
            Assert.AreEqual("5", testSett.Transitions[0].direction);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Test_editTrans_2()
        {
            initSett();
            int param1 = testSett.Transitions.Count + 5;
            string param2 = "1\n2\n\n3\n4\n5";
            testSett.editTrans(param1, param2);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Test_editTrans_3()
        {
            initSett();
            
            string param2 = "1\n2\n";
            testSett.editTrans(0, param2);
        }

        public void isSettValidHelper(ref string x)
        {
            string temp = x;
            x = null;
            Assert.AreEqual(false, testSett.isSettValid());
            x = "";
            Assert.AreEqual(false, testSett.isSettValid());
            x = temp;
            Assert.AreEqual(true, testSett.isSettValid());
        }

        [TestMethod]
        public void Test_isSettValid_1()
        {
            initSett();

            isSettValidHelper(ref testSett.StateType);
            isSettValidHelper(ref testSett.AlphabetType);
            isSettValidHelper(ref testSett.StartState);
            isSettValidHelper(ref testSett.Carret);

            string temp = testSett.Tape[0];
            testSett.Tape[0] = null;
            Assert.AreEqual(false, testSett.isSettValid());
            testSett.Tape[0] = "";
            Assert.AreEqual(false, testSett.isSettValid());
            testSett.Tape[0] = temp;
            Assert.AreEqual(true, testSett.isSettValid());

            temp = testSett.Transitions[0].state;
            testSett.Transitions[0].state = null;
            Assert.AreEqual(false, testSett.isSettValid());
            testSett.Transitions[0].state = "";
            Assert.AreEqual(false, testSett.isSettValid());
            testSett.Transitions[0].state = temp;
            Assert.AreEqual(true, testSett.isSettValid());

            temp = testSett.Transitions[0].value;
            testSett.Transitions[0].value = null;
            Assert.AreEqual(false, testSett.isSettValid());
            testSett.Transitions[0].value = "";
            Assert.AreEqual(false, testSett.isSettValid());
            testSett.Transitions[0].value = temp;
            Assert.AreEqual(true, testSett.isSettValid());

            temp = testSett.Transitions[0].nextState;
            testSett.Transitions[0].nextState = null;
            Assert.AreEqual(false, testSett.isSettValid());
            testSett.Transitions[0].nextState = "";
            Assert.AreEqual(false, testSett.isSettValid());
            testSett.Transitions[0].nextState = temp;
            Assert.AreEqual(true, testSett.isSettValid());

            temp = testSett.Transitions[0].newValue;
            testSett.Transitions[0].newValue = null;
            Assert.AreEqual(false, testSett.isSettValid());
            testSett.Transitions[0].newValue = "";
            Assert.AreEqual(false, testSett.isSettValid());
            testSett.Transitions[0].newValue = temp;
            Assert.AreEqual(true, testSett.isSettValid());

            temp = testSett.Transitions[0].direction;
            testSett.Transitions[0].direction = null;
            Assert.AreEqual(false, testSett.isSettValid());
            testSett.Transitions[0].direction = "";
            Assert.AreEqual(false, testSett.isSettValid());
            testSett.Transitions[0].direction = temp;
            Assert.AreEqual(true, testSett.isSettValid());
        }
    }
}
