﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TuringMachine;
using System.Windows;

namespace TuringMachineTest
{
    [TestClass]
    public class TMtest
    {
        public TuringMachine<string, string> testTM;

        public void initTM()
        {
            testTM = new TuringMachine<string,string>();
            testTM.actualState="0";
            testTM.Carret=0;
            testTM.Tape.Add("a");
            testTM.Tape.Add("b");
            testTM += new Tuple<string, string, string, string, int>("0", "a", "1", "b", 1);
            testTM += new Tuple<string, string, string, string, int>("0", "b", "1", "a", 1);
            testTM += new Tuple<string, string, string, string, int>("1", "a", "1", "b", -1);
            testTM += new Tuple<string, string, string, string, int>("1", "b", "0", "a", -1);
            testTM.transToDisp = 0;
        }

        [TestMethod]
        public void Test_ConstructorSettings1_1()
        {
            Settings1 tempSett = new Settings1();
            tempSett.StartState = "0";
            tempSett.Carret = "1";
            tempSett.Tape.Add("2");
            tempSett += new Tuple<string, string, string, string, string>("3", "4", "5", "6", "7");

            testTM = new TuringMachine<string, string>(tempSett);

            Assert.AreEqual(tempSett.StartState, testTM.actualState, "TM.ActualState wrong value!");
            Assert.AreEqual(System.Convert.ToInt32(tempSett.Carret), testTM.Carret, "TM.Carret wrong value!");
            Assert.AreEqual(tempSett.Tape[0], testTM.Tape[0], "TM.Tape[0] wrong value!");
            Assert.AreEqual(tempSett.Transitions[0].state, testTM.Transitions[0].state, "TM.Transition.state wrong value!");
            Assert.AreEqual(tempSett.Transitions[0].value, testTM.Transitions[0].value, "TM.Transition.value wrong value!");
            Assert.AreEqual(tempSett.Transitions[0].nextState, testTM.Transitions[0].nextState, "TM.Transition.nextState wrong value!");
            Assert.AreEqual(tempSett.Transitions[0].newValue, testTM.Transitions[0].newValue, "TM.Transition.newValue wrong value!");
            Assert.AreEqual(System.Convert.ToInt32(tempSett.Transitions[0].direction), testTM.Transitions[0].direction, "TM.Transition.direction wrong value!");
        }

        [TestMethod]
        public void Test_Step_1()
        {
            initTM();
            testTM.Step();
            Assert.AreEqual(testTM.actualState, "1", "actualState not correct!");
            Assert.AreEqual(testTM.Carret, 1, "Carret not correct!");
            Assert.AreEqual(testTM.Tape[0], "b", "Tape value not correct!");
            Assert.AreEqual(testTM.transToDisp, 0, "actualState not correct!");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Test_Step_2()
        {
            initTM();
            testTM.Transitions.Clear();
            testTM.Step();
        }

        //TestStep_3 Bad Equals Overload

        [TestMethod]
        public void Test_operator_1()
        {
            initTM();
            Tuple<string, string, string, string, int> tempTuple = new Tuple<string, string, string, string, int>("a", "b", "c", "d", 10);
            testTM += tempTuple;
            Assert.AreEqual(testTM.Transitions[testTM.Transitions.Count - 1].state, tempTuple.Item1, "Transition.state bad value!");
            Assert.AreEqual(testTM.Transitions[testTM.Transitions.Count - 1].value, tempTuple.Item2, "Transition.value bad value!");
            Assert.AreEqual(testTM.Transitions[testTM.Transitions.Count - 1].nextState, tempTuple.Item3, "Transition.nextState bad value!");
            Assert.AreEqual(testTM.Transitions[testTM.Transitions.Count - 1].newValue, tempTuple.Item4, "Transition.newValue bad value!");
            Assert.AreEqual(testTM.Transitions[testTM.Transitions.Count - 1].direction, tempTuple.Item5, "Transition.direction bad value!");
        }

        [TestMethod]
        public void Test_foreach_1()
        {
            initTM();
            string temp1 = "";
            string temp2 = "";

            foreach (string s in testTM.Tape)
            {
                temp1 += s;
            }

            foreach (string s in testTM)
            {
                temp2 += s;
            }

            Assert.AreEqual(temp1, temp2,"Two loops returns different values");
        }
    }
}
