﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GUI;
using TuringMachine;

namespace TuringMachineTest
{
    [TestClass]
    public class AssemblySettingsTest
    {
        public AssemblySettings testASett;

        [TestMethod]
        public void Test_read_write_XML()
        {
            testASett = new AssemblySettings();
            Settings1 temp1sett = new Settings1();
            temp1sett.StartState = "0";
            temp1sett.Carret = "0";
            temp1sett.Tape.Add("a");
            temp1sett.Tape.Add("b");
            temp1sett += new Tuple<string, string, string, string, string>("0", "a", "1", "b", "1");
            temp1sett += new Tuple<string, string, string, string, string>("0", "b", "1", "a", "1");
            temp1sett += new Tuple<string, string, string, string, string>("1", "a", "1", "b", "-1");
            temp1sett += new Tuple<string, string, string, string, string>("1", "b", "0", "a", "-1");

            temp1sett.AlphabetType = "char";
            temp1sett.StateType = "int";

            AssemblySettings.writeXML("C:\\Windows\\temp\\TestXML.xml", temp1sett);

            Settings1 temp2sett = new Settings1();

            testASett.readXML("C:\\Windows\\temp\\TestXML.xml", temp2sett);

            Assert.AreEqual(temp1sett.StateType, temp2sett.StateType);
            Assert.AreEqual(temp1sett.AlphabetType, temp2sett.AlphabetType);
            Assert.AreEqual(temp1sett.StartState, temp2sett.StartState);
            Assert.AreEqual(temp1sett.Carret, temp2sett.Carret);
            Assert.AreEqual(temp1sett.assemblyPath, temp2sett.assemblyPath);

            int x = temp1sett.Tape.Count > temp2sett.Tape.Count ? temp1sett.Tape.Count : temp2sett.Tape.Count;
            for (int i = 0; i < x; i++)
            {
                Assert.AreEqual(temp1sett.Tape[i], temp2sett.Tape[i]);
            }

            x = temp1sett.Transitions.Count > temp2sett.Transitions.Count ? temp1sett.Transitions.Count : temp2sett.Transitions.Count;
            for (int i = 0; i < x; i++)
            {
                Assert.AreEqual(temp1sett.Transitions[i].state, temp2sett.Transitions[i].state);
                Assert.AreEqual(temp1sett.Transitions[i].value, temp2sett.Transitions[i].value);
                Assert.AreEqual(temp1sett.Transitions[i].nextState, temp2sett.Transitions[i].nextState);
                Assert.AreEqual(temp1sett.Transitions[i].newValue, temp2sett.Transitions[i].newValue);
                Assert.AreEqual(temp1sett.Transitions[i].direction, temp2sett.Transitions[i].direction);
            }

            System.IO.File.Delete("C:\\Windows\\temp\\TestXML.xml");
        }

        //parseTM
    }
}
