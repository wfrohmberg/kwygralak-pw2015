﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.IO.Compression;
using TuringMachine;
using System.Reflection;

namespace GUI
{
    public partial class AssemblySettings : Form
    {
        Settings1 sett;
        Settings1 paramSett;
        string assemblyPath = "";

        Assembly loadedAssembly;

        TuringMachine<object, object> TM;
        int transToShow = 0;

        public bool Apply = false;

        public AssemblySettings()
        {
            InitializeComponent();
        }
        
        public AssemblySettings(TuringMachine.Settings1 param1, TuringMachine<object,object> param2)
        {
            InitializeComponent();
            sett = new Settings1();
            resetComboBoxes();
            comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox2.DropDownStyle = ComboBoxStyle.DropDownList;
            tbPath.Enabled = false;
            paramSett = param1;
            sett.duplicateSett(param1);
            button2.Enabled = false;
            button3.Enabled = false;
            richTextBox1.Enabled = false;
            if (sett.assemblyPath.Length >= 5)
            {
                loadedAssembly = Assembly.LoadFile(sett.assemblyPath);
                foreach (Type type in loadedAssembly.GetExportedTypes())
                {
                    comboBox1.Items.Add(type.ToString());
                    comboBox2.Items.Add(type.ToString());
                }
            }
            dispSettings();
            if (sett.Transitions.Count >= 2)
            {
                button3.Enabled = true;
            }
            TM = param2;
        }
        
        public void resetComboBoxes()
        {
            comboBox1.Items.Clear();
            comboBox2.Items.Clear();
            comboBox1.Items.Add("int");
            comboBox1.Items.Add("char");
            comboBox1.Items.Add("string");
            comboBox1.Items.Add("float");
            comboBox2.Items.Add("int");
            comboBox2.Items.Add("char");
            comboBox2.Items.Add("string");
            comboBox2.Items.Add("float");
        }
        
        public void dispSettings()
        {
            comboBox1.SelectedItem = sett.AlphabetType;
            comboBox2.SelectedItem = sett.StateType;
            textBox1.Clear();
            textBox1.Text = sett.Carret;
            textBox2.Clear();
            textBox2.Text = sett.StartState;
            tbPath.Text = sett.assemblyPath;
            richTextBox2.Clear();

            foreach (string x in sett.Tape)
            {
                richTextBox2.Text += x + "\n";
            }
            richTextBox1.Clear();
            if (sett.Transitions.Count >= 1)
            {
                btEditTrans.Enabled = true;
                Settings1.Transition temp = sett.Transitions.ElementAt(transToShow);
                richTextBox1.Text = temp.state + "\n" + temp.value + "\n\n" + temp.nextState
                                    + "\n" + temp.newValue + "\n" + temp.direction;
            }
            else
            {
                btEditTrans.Enabled = false;
            }
        }

        public void readXML(string path, Settings1 param2)
        {
            param2.Transitions.Clear();
            param2.Tape.Clear();
            FileStream stream = new FileStream(path, FileMode.Open);

            XmlReaderSettings readerSettings = new XmlReaderSettings();
            readerSettings.IgnoreWhitespace = true;
            XmlReader reader = XmlTextReader.Create(stream, readerSettings);


            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element)
                {
                    switch (reader.Name)
                    {
                        case "path":
                            {
                                reader.Read();
                                assemblyPath=reader.Value;
                                param2.assemblyPath = reader.Value.ToString();
                                reader.Read();
                            }break;
                        case "transition":
                            {
                                string state = "", nextState = "", direction = "";
                                string value = "", newValue = "";
                                reader.Read();
                                while (reader.NodeType != XmlNodeType.EndElement && reader.Name != "transition")
                                {
                                    switch (reader.Name)
                                    {
                                        case "state":
                                            reader.Read();
                                            //Console.WriteLine("W state " + reader.NodeType + " " + reader.Name + " " + reader.Value);
                                            state = reader.Value.ToString();
                                            break;
                                        case "value":
                                            reader.Read();
                                            //Console.WriteLine("W value " + reader.NodeType + " " + reader.Name + " " + reader.Value);
                                            value = reader.Value.ToString();

                                            break;
                                        case "nextState":
                                            reader.Read();
                                            //Console.WriteLine("W nextstate " + reader.NodeType + " " + reader.Name + " " + reader.Value);
                                            nextState = reader.Value.ToString();

                                            break;
                                        case "newValue":
                                            reader.Read();
                                            //Console.WriteLine("W newvalue " + reader.NodeType + " " + reader.Name + " " + reader.Value);
                                            newValue = reader.Value.ToString();

                                            break;
                                        case "direction":
                                            reader.Read();
                                            //Console.WriteLine("W direction " + reader.NodeType + " " + reader.Name + " " + reader.Value);
                                            direction = reader.Value.ToString();

                                            break;
                                    }
                                    reader.Read();
                                    reader.Read();

                                }
                                param2 += new Tuple<string, string, string, string, string>(state, value, nextState, newValue, direction);
                            } break;

                        case "carret":
                            {
                                reader.Read();
                                param2.Carret = reader.Value.ToString();
                                reader.Read();
                            } break;

                        case "AlphabetType":
                            {
                                
                                reader.Read();
                                //Console.WriteLine("W AT " + reader.NodeType + " " + reader.Name + " " + reader.Value);
                                param2.AlphabetType = reader.Value.ToString();
                                reader.Read();
                            } break;

                        case "StateType":
                            {
                                
                                reader.Read();
                                //Console.WriteLine("W ST " + reader.NodeType + " " + reader.Name + " " + reader.Value);
                                param2.StateType = reader.Value.ToString();
                                reader.Read();
                            } break;

                        case "StartState":
                            {
                                reader.Read();
                                param2.StartState = reader.Value.ToString();
                                reader.Read();
                            } break;

                        case "tape":
                            {
                                //Console.WriteLine("W tape " + reader.NodeType + " " + reader.Name + " " + reader.Value);
                                reader.Read();
                                do
                                {
                                    //Console.WriteLine("W tapeelem " + reader.NodeType + " " + reader.Name + " " + reader.Value);
                                    
                                    reader.Read();
                                    param2.Tape.Add(reader.Value.ToString());
                                    reader.Read();
                                    reader.Read();
                                } while (reader.Name != "tape");
                            } break;
                    }

                }
            }
            reader.Close();
            stream.Close();
        }

        public static void writeXML(string path, Settings1 param2)
        {
            FileStream stream = new FileStream(path, FileMode.Create);

            XmlWriterSettings writerSettings = new XmlWriterSettings
            {
                Indent = true,
                IndentChars = "  ",
                NewLineChars = "\r\n",
                NewLineHandling = NewLineHandling.Replace
            };

            XmlWriter writer = XmlWriter.Create(stream, writerSettings);

            writer.WriteStartElement("body");
            if (param2.AlphabetType != "")
            {
                writer.WriteStartElement("AlphabetType");
                writer.WriteString(param2.AlphabetType);
                writer.WriteEndElement();
            }
            if (param2.StateType != "")
            {
                writer.WriteStartElement("StateType");
                writer.WriteString(param2.StateType);
                writer.WriteEndElement();
            }
            if (param2.StartState != "")
            {
                writer.WriteStartElement("StartState");
                writer.WriteString(param2.StartState);
                writer.WriteEndElement();
            }
            if (param2.Carret != "")
            {
                writer.WriteStartElement("carret");
                writer.WriteString(param2.Carret);
                writer.WriteEndElement();
            }
            if (param2.assemblyPath != "")
            {
                writer.WriteStartElement("path");
                writer.WriteString(param2.assemblyPath);
                writer.WriteEndElement();
            }
            if (param2.Tape.Count >= 1)
            {
                writer.WriteStartElement("tape");
                foreach (string x in param2.Tape)
                {
                    writer.WriteStartElement("tapeelem");
                    writer.WriteString(x);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }

            foreach (Settings1.Transition i in param2.Transitions)
            {
                writer.WriteStartElement("transition");
                writer.WriteStartElement("state");
                writer.WriteString(i.state.ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("value");
                writer.WriteString(i.value.ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("nextState");
                writer.WriteString(i.nextState.ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("newValue");
                writer.WriteString(i.newValue.ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("direction");
                writer.WriteString(i.direction.ToString());
                writer.WriteEndElement();

                writer.WriteEndElement();

            }

            writer.WriteEndElement();

            writer.Close();
            stream.Close();
        }

        public void updateSett()
        {
            sett.AlphabetType = comboBox1.Text;
            sett.StateType = comboBox2.Text;
            sett.Carret = textBox1.Text;
            sett.StartState = textBox2.Text;
            sett.Tape.Clear();
            sett.assemblyPath = tbPath.Text;
            string[] temp = richTextBox2.Text.Split('\n');
            foreach (string x in temp)
            {
                //Console.WriteLine("xx" + x + "xx");
                if (x.Length >= 1)
                {
                    sett.Tape.Add(x);
                }
            }
        }

        private void /*LoadXML*/button4_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = "C:\\Users\\Kamil\\Desktop\\";
            openFileDialog1.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                sett.assemblyPath = "";
                resetComboBoxes();
                readXML(openFileDialog1.FileName, sett);
                //tbPath.Text = openFileDialog1.FileName;
                transToShow = 0;
                button2.Enabled = false;
                button3.Enabled = sett.Transitions.Count() >= 2 ? true : false;
                if (sett.assemblyPath.Length >= 3)
                {
                    try
                    {
                        loadedAssembly = Assembly.LoadFile(sett.assemblyPath);
                    }
                    catch(FileNotFoundException)
                    {
                        MessageBox.Show("Assembly file not found!", "Important Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        sett.assemblyPath = "";
                        dispSettings();
                        return;
                    }
                    foreach (Type type in loadedAssembly.GetExportedTypes())
                    {
                        comboBox1.Items.Add(type.ToString());
                        comboBox2.Items.Add(type.ToString());
                    }
                }
                dispSettings();
            }
        }

        private void /* < */button2_Click(object sender, EventArgs e)
        {
            if (transToShow >= 1)
            {
                transToShow--;
                updateSett();
                dispSettings();
                if (transToShow == 0)
                {
                    button2.Enabled = false;
                }
                button3.Enabled = true;
            }
        }

        private void /* > */button3_Click(object sender, EventArgs e)
        {
            if (transToShow < sett.Transitions.Count)
            {
                transToShow++;
                updateSett();
                dispSettings();
                if (transToShow == sett.Transitions.Count-1)
                {
                    button3.Enabled = false;
                }
                button2.Enabled = true;
            }
        }

        private void btAddTrans_Click(object sender, EventArgs e)
        {
            sett += new Tuple<string, string, string, string, string>("-1", "-1", "-1", "-1", "-1");
            if (sett.Transitions.Count >= 2)
            {
                button3.Enabled = true;
            }
            updateSett();
            dispSettings();
        }

        private void btRemTrans_Click(object sender, EventArgs e)
        {
            if (sett.Transitions.Count > 0)
            {
                sett.Transitions.RemoveAt(transToShow);
                transToShow = 0;
                button2.Enabled = false;
                button3.Enabled = sett.Transitions.Count() >= 2 ? true : false;
                updateSett();
                dispSettings();
            }
        }

        private void btEditTrans_Click(object sender, EventArgs e)
        {
            if (btEditTrans.Text == "Edit")
            {
                richTextBox1.Enabled = true;
                btAddTrans.Enabled = false;
                btRemTrans.Enabled = false;
                button2.Enabled = false;
                button3.Enabled = false;
                btEditTrans.Text = "Save";
            }
            else
            {
                try
                {
                    sett.editTrans(transToShow, richTextBox1.Text);
                    richTextBox1.Enabled = false;
                    btAddTrans.Enabled = true;
                    btRemTrans.Enabled = true;
                    if (transToShow != 0)
                    {
                        button2.Enabled = true;
                    }
                    if (transToShow < sett.Transitions.Count - 1)
                    {
                        button3.Enabled = true;
                    }
                    btEditTrans.Text = "Edit";
                }
                catch (ArgumentOutOfRangeException)
                {
                    MessageBox.Show("Transition have bad values!", "Important Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

        }
        
        public Type getType(string s)
        {
            switch (s)
            {
                case "int": return typeof(int); 
                case "float": return typeof(float); 
                case "char": return typeof(char); 
                case "string": return typeof(string);
                default: return null;
            }
        }

        public void parseTM()
        {
            List<object> transValues = new List<object>();
            List<object> transNewValues = new List<object>();
            TM.Tape.Clear();
            TM.Transitions.Clear();

                //ALPHABET TYPE
                if (getType(comboBox1.Text) == null)
                {
                    Type type = null;
                    foreach (Type t in loadedAssembly.GetExportedTypes())
                    {
                        if (t.ToString() == comboBox1.Text)
                        {
                            type = t;
                        }
                    }
                    //TAPE
                    foreach (string s in sett.Tape)
                    {
                        TM.Tape.Add(Activator.CreateInstance(type, new object[] { s }));
                    }
                    //TRANS
                    foreach (Settings1.Transition x in sett.Transitions)
                    {
                        transValues.Add(Activator.CreateInstance(type, new object[] { x.value }));
                        transNewValues.Add(Activator.CreateInstance(type, new object[] { x.newValue }));
                    }
                }
                else
                {
                    foreach (string s in sett.Tape)
                    {
                        TM.Tape.Add(System.Convert.ChangeType(s, getType(comboBox1.Text)));
                    }
                    foreach (Settings1.Transition x in sett.Transitions)
                    {
                        transValues.Add(System.Convert.ChangeType(x.value, getType(comboBox1.Text)));
                        transNewValues.Add(System.Convert.ChangeType(x.newValue, getType(comboBox1.Text)));
                    }
                }

                //STATE TYPE
                if (getType(comboBox2.Text) == null)
                {
                    Type type = null;
                    foreach (Type t in loadedAssembly.GetExportedTypes())
                    {
                        if (t.ToString() == comboBox1.Text)
                        {
                            type = t;
                        }
                    }

                    TM.actualState = Activator.CreateInstance(type, new object[] { sett.StartState });
                    int i = 0;
                    foreach (Settings1.Transition x in sett.Transitions)
                    {
                        TM += new Tuple<object, object, object, object, int>(
                            Activator.CreateInstance(type, new object[] { x.state }),
                            transValues[i],
                            Activator.CreateInstance(type, new object[] { x.nextState }),
                            transNewValues[i],
                            (int)System.Convert.ChangeType(x.direction, typeof(int))
                            );
                        i++;
                    }
                }
                else
                {
                    TM.actualState = System.Convert.ChangeType(sett.StartState, getType(comboBox2.Text));
                    int i = 0;
                    foreach (Settings1.Transition x in sett.Transitions)
                    {
                        TM += new Tuple<object, object, object, object, int>(
                            System.Convert.ChangeType(x.state, getType(comboBox2.Text)),
                            transValues[i],
                            System.Convert.ChangeType(x.nextState, getType(comboBox2.Text)),
                            transNewValues[i],
                            (int)System.Convert.ChangeType(x.direction, typeof(int))
                            );
                        i++;
                    }
                }
                TM.Carret = (int)System.Convert.ChangeType(sett.Carret, typeof(int));

        }

        private void /*Apply*/button1_Click(object sender, EventArgs e)
        {
            updateSett();
            if (sett.isSettValid())
            {
                
                try{
                    parseTM();
                    paramSett.duplicateSett(sett);
                    Console.WriteLine("NIE MA EXCEPTION");
                    Apply = true;
                    Dispose();
                }
                catch (Exception)
                {
                    MessageBox.Show("Some parameters have wrong values!", "Important Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Some parameters are empty!", "Important Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void /*SaveXML*/button5_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.InitialDirectory = "C:\\Users\\Kamil\\Desktop\\";
            saveFileDialog1.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                updateSett();
                writeXML(saveFileDialog1.FileName, sett);
            }
        }

        private void btLoadAssembly_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = "C:\\Users\\Kamil\\Desktop\\";
            openFileDialog1.Filter = "dll files (*.dll)|*.dll|All files (*.*)|*.*";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                sett.assemblyPath = openFileDialog1.FileName;
                tbPath.Text = openFileDialog1.FileName;
                loadedAssembly = Assembly.LoadFile(sett.assemblyPath);
                foreach (Type type in loadedAssembly.GetExportedTypes())
                {
                    comboBox1.Items.Add(type.ToString());
                    comboBox2.Items.Add(type.ToString());
                }
            }
        }

        private void btSaveZIP_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.InitialDirectory = "C:\\Users\\Kamil\\Desktop\\";
            saveFileDialog1.Filter = "zip files (*.zip)|*.zip|All files (*.*)|*.*";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                updateSett();
                if(File.Exists(saveFileDialog1.FileName))
                {
                    File.Delete(saveFileDialog1.FileName);
                }
                using (ZipArchive zip = ZipFile.Open(saveFileDialog1.FileName, ZipArchiveMode.Create))
                {
                    try
                    {
                        string tempDLLname = loadedAssembly.GetExportedTypes()[0].ToString().Split('.')[0];
                        zip.CreateEntryFromFile(sett.assemblyPath, tempDLLname + ".dll");

                        string tempPath = "C:\\Windows\\temp\\temp.xml";
                        writeXML(tempPath, sett);
                        zip.CreateEntryFromFile(tempPath, tempDLLname + ".xml");
                        File.Delete(tempPath);
                    }
                    catch (System.IO.IOException)
                    {
                        MessageBox.Show("Some of files was invalid!", "Important Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    catch (NullReferenceException)
                    {
                        MessageBox.Show("There are no DLL loaded!", "Important Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                
            }

        }

    }
}
