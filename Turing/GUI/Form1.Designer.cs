﻿namespace GUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                
                components.Dispose();
            }
            //System.Console.WriteLine("DisposeComponents");
            Animation.Abort();
            
            base.Dispose(disposing);

        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.btStart = new System.Windows.Forms.Button();
            this.tbDelay = new System.Windows.Forms.TextBox();
            this.labelDelay = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.btRestart = new System.Windows.Forms.Button();
            this.btAssemblySettings = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Location = new System.Drawing.Point(10, 10);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(250, 250);
            this.panel1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(502, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Settings";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btStart
            // 
            this.btStart.Location = new System.Drawing.Point(378, 238);
            this.btStart.Name = "btStart";
            this.btStart.Size = new System.Drawing.Size(75, 23);
            this.btStart.TabIndex = 4;
            this.btStart.Text = "Start";
            this.btStart.UseVisualStyleBackColor = true;
            this.btStart.Click += new System.EventHandler(this.btStart_Click);
            // 
            // tbDelay
            // 
            this.tbDelay.Location = new System.Drawing.Point(287, 240);
            this.tbDelay.Name = "tbDelay";
            this.tbDelay.Size = new System.Drawing.Size(66, 20);
            this.tbDelay.TabIndex = 5;
            this.tbDelay.Text = "800";
            // 
            // labelDelay
            // 
            this.labelDelay.AutoSize = true;
            this.labelDelay.Location = new System.Drawing.Point(284, 224);
            this.labelDelay.Name = "labelDelay";
            this.labelDelay.Size = new System.Drawing.Size(62, 13);
            this.labelDelay.TabIndex = 6;
            this.labelDelay.Text = "Delay [ms] :";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(287, 35);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(166, 108);
            this.richTextBox1.TabIndex = 7;
            this.richTextBox1.Text = "";
            // 
            // btRestart
            // 
            this.btRestart.Location = new System.Drawing.Point(459, 237);
            this.btRestart.Name = "btRestart";
            this.btRestart.Size = new System.Drawing.Size(75, 23);
            this.btRestart.TabIndex = 8;
            this.btRestart.Text = "Restart";
            this.btRestart.UseVisualStyleBackColor = true;
            this.btRestart.Click += new System.EventHandler(this.btRestart_Click);
            // 
            // btAssemblySettings
            // 
            this.btAssemblySettings.Location = new System.Drawing.Point(502, 41);
            this.btAssemblySettings.Name = "btAssemblySettings";
            this.btAssemblySettings.Size = new System.Drawing.Size(75, 23);
            this.btAssemblySettings.TabIndex = 9;
            this.btAssemblySettings.Text = "Assembly";
            this.btAssemblySettings.UseVisualStyleBackColor = true;
            this.btAssemblySettings.Click += new System.EventHandler(this.btAssemblySettings_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(589, 289);
            this.Controls.Add(this.btAssemblySettings);
            this.Controls.Add(this.btRestart);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.labelDelay);
            this.Controls.Add(this.tbDelay);
            this.Controls.Add(this.btStart);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btStart;
        private System.Windows.Forms.TextBox tbDelay;
        private System.Windows.Forms.Label labelDelay;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button btRestart;
        private System.Windows.Forms.Button btAssemblySettings;

    }
}

