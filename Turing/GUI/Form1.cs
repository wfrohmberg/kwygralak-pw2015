﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using TuringMachine;

namespace GUI
{

    
    public partial class Form1 : Form
    {
        public Graphics panel1draw;
        //Object TM;
        //public TuringMachine<char, int> TM = new TuringMachine<char, int>();
        public Settings1 sett = new Settings1();
        public TuringAbstract TM;
        TuringMachine<object, object> TuringObject = new TuringMachine<object, object>();
        //TuringMachine<char, int> TM2 = new TuringMachine<char, int>();
        public Pen pen = new Pen(Color.FromArgb(255, 0, 0, 0));

        Thread Animation;
        bool AnimationActive = false;

        public void threadFunc()
        {
            while (true)
            {
                if (AnimationActive)
                {
                    try
                    {
                        TM.redraw(panel1draw, pen);
                        TM.Step();
                        this.Invoke(
                                new Action(() =>
                                {
                                    TM.dispalyTransition(richTextBox1);
                                }));
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        MessageBox.Show("Transition not found!", "Important Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        AnimationActive = false;
                    }
                }
                Thread.Sleep(System.Convert.ToInt32(tbDelay.Text));
            }
        }

        public void createTuringMachine()
        {
            switch (sett.AlphabetType + sett.StateType)
            {
                case "intint": TM = new TuringMachine<int, int>(sett); break;
                case "intchar": TM = new TuringMachine<int, char>(sett); break;
                case "intfloat": TM = new TuringMachine<int, float>(sett); break;
                case "intstring": TM = new TuringMachine<int, string>(sett); break;
                case "charint": TM = new TuringMachine<char, int>(sett); break;
                case "charchar": TM = new TuringMachine<char, char>(sett); break;
                case "charfloat": TM = new TuringMachine<char, float>(sett); break;
                case "charstring": TM = new TuringMachine<char, string>(sett); break;
                case "floatint": TM = new TuringMachine<float, int>(sett); break;
                case "floatchar": TM = new TuringMachine<float, char>(sett); break;
                case "floatfloat": TM = new TuringMachine<float, float>(sett); break;
                case "floatstring": TM = new TuringMachine<float, string>(sett); break;
                case "stringint": TM = new TuringMachine<string, int>(sett); break;
                case "stringchar": TM = new TuringMachine<string, char>(sett); break;
                case "stringfloat": TM = new TuringMachine<string, float>(sett); break;
                case "stringstring": TM = new TuringMachine<string, string>(sett); break;
                default: TM = TuringObject; break;
            }
        }
        
        public Form1()
        {
            InitializeComponent();
            panel1draw = panel1.CreateGraphics();
            //initTM();
            Animation = new Thread(threadFunc);
            Animation.Start();
        }
        
        private void /*Settings*/button1_Click(object sender, EventArgs e)
        {
            Settings form = new Settings(sett);
            form.ShowDialog();
        }

        private void btStart_Click(object sender, EventArgs e)
        {
            //AnimationActive = AnimationActive ? false : true;
            
            //createTuringMachine();
            
            if (btStart.Text == "Start")
            {
                if (sett.AlphabetType == null)
                {
                    MessageBox.Show("No data!", "Important Message", MessageBoxButtons.OK,MessageBoxIcon.Error);
                }
                else
                {
                    createTuringMachine();
                    tbDelay.Enabled = false;
                    btStart.Text = "Pause";
                    AnimationActive = true;
                    button1.Enabled = false;
                    btAssemblySettings.Enabled = false;
                }
            }
            else if (btStart.Text == "Pause")
            {
                tbDelay.Enabled = true;
                btStart.Text = "Continue";
                AnimationActive = false;
            }
            else if (btStart.Text == "Continue")
            {
                tbDelay.Enabled = false;
                btStart.Text = "Pause";
                AnimationActive = true;
            }
        }

        private void btRestart_Click(object sender, EventArgs e)
        {
            panel1draw.Clear(System.Drawing.Color.White);
            richTextBox1.Clear();
            tbDelay.Enabled = true;
            btStart.Text = "Start";
            AnimationActive = false;
            button1.Enabled = true;
            btAssemblySettings.Enabled = true;
        }

        private void btAssemblySettings_Click(object sender, EventArgs e)
        {
            TuringMachine<object, object> tempTM = new TuringMachine<object, object>();
            AssemblySettings form = new AssemblySettings(sett, tempTM);
            //TM = temp;
            form.ShowDialog();
            if (form.Apply)
            {
                TuringObject = tempTM;
            }
            tempTM = null;
        }
    }
}
