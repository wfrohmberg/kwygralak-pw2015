﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using TuringMachine;

namespace GUI
{
    public partial class Settings : Form
    {
        Settings1 sett = new Settings1();
        Settings1 paramSett;
        int transToShow = 0;

        public Settings()
        {
            InitializeComponent();
        }
        public Settings(TuringMachine.Settings1 param1)//, TuringAbstract param2)
        {
            InitializeComponent();
            comboBox1.Items.Add("int");
            comboBox1.Items.Add("char");
            comboBox1.Items.Add("string");
            comboBox1.Items.Add("float");
            comboBox2.Items.Add("int");
            comboBox2.Items.Add("char");
            comboBox2.Items.Add("string");
            comboBox2.Items.Add("float");
            paramSett = param1;
            sett.duplicateSett(param1);
            button2.Enabled = false;
            button3.Enabled = false;
            richTextBox1.Enabled = false;
            dispSettings();
            if (sett.Transitions.Count >= 2)
            {
                button3.Enabled = true;
            }
        }
        
        public void dispSettings()
        {

            comboBox1.SelectedItem = sett.AlphabetType;
            comboBox2.SelectedItem = sett.StateType;
            textBox1.Clear();
            textBox1.Text = sett.Carret;
            textBox2.Clear();
            textBox2.Text = sett.StartState;
            richTextBox2.Clear();

            foreach (string x in sett.Tape)
            {
                richTextBox2.Text += x + "\n";
            }
            richTextBox1.Clear();
            if (sett.Transitions.Count >= 1)
            {
                btEditTrans.Enabled = true;
                Settings1.Transition temp = sett.Transitions.ElementAt(transToShow);
                richTextBox1.Text = temp.state + "\n" + temp.value + "\n\n" + temp.nextState
                                    + "\n" + temp.newValue + "\n" + temp.direction;
            }
            else
            {
                btEditTrans.Enabled = false;
            }

        }

        public static void readXML(string path, Settings1 param2)
        {
            param2.Transitions.Clear();
            param2.Tape.Clear();
            FileStream stream = new FileStream(path, FileMode.Open);

            XmlReaderSettings readerSettings = new XmlReaderSettings();
            readerSettings.IgnoreWhitespace = true;
            XmlReader reader = XmlTextReader.Create(stream, readerSettings);


            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element)
                {
                    switch (reader.Name)
                    {
                        case "transition":
                            {
                                string state = "", nextState = "", direction = "";
                                string value = "", newValue = "";
                                reader.Read();
                                while (reader.NodeType != XmlNodeType.EndElement && reader.Name != "transition")
                                {
                                    switch (reader.Name)
                                    {
                                        case "state":
                                            reader.Read();
                                            //Console.WriteLine("W state " + reader.NodeType + " " + reader.Name + " " + reader.Value);
                                            state = reader.Value.ToString();
                                            break;
                                        case "value":
                                            reader.Read();
                                            //Console.WriteLine("W value " + reader.NodeType + " " + reader.Name + " " + reader.Value);
                                            value = reader.Value.ToString();

                                            break;
                                        case "nextState":
                                            reader.Read();
                                            //Console.WriteLine("W nextstate " + reader.NodeType + " " + reader.Name + " " + reader.Value);
                                            nextState = reader.Value.ToString();

                                            break;
                                        case "newValue":
                                            reader.Read();
                                            //Console.WriteLine("W newvalue " + reader.NodeType + " " + reader.Name + " " + reader.Value);
                                            newValue = reader.Value.ToString();

                                            break;
                                        case "direction":
                                            reader.Read();
                                            //Console.WriteLine("W direction " + reader.NodeType + " " + reader.Name + " " + reader.Value);
                                            direction = reader.Value.ToString();

                                            break;
                                    }
                                    reader.Read();
                                    reader.Read();

                                }
                                param2 += new Tuple<string, string, string, string, string>(state, value, nextState, newValue, direction);
                            } break;

                        case "carret":
                            {
                                reader.Read();
                                param2.Carret = reader.Value.ToString();
                                reader.Read();
                            } break;

                        case "AlphabetType":
                            {
                                
                                reader.Read();
                                //Console.WriteLine("W AT " + reader.NodeType + " " + reader.Name + " " + reader.Value);
                                param2.AlphabetType = reader.Value.ToString();
                                reader.Read();
                            } break;

                        case "StateType":
                            {
                                
                                reader.Read();
                                //Console.WriteLine("W ST " + reader.NodeType + " " + reader.Name + " " + reader.Value);
                                param2.StateType = reader.Value.ToString();
                                reader.Read();
                            } break;

                        case "StartState":
                            {
                                reader.Read();
                                param2.StartState = reader.Value.ToString();
                                reader.Read();
                            } break;

                        case "tape":
                            {
                                //Console.WriteLine("W tape " + reader.NodeType + " " + reader.Name + " " + reader.Value);
                                reader.Read();
                                do
                                {
                                    //Console.WriteLine("W tapeelem " + reader.NodeType + " " + reader.Name + " " + reader.Value);
                                    
                                    reader.Read();
                                    param2.Tape.Add(reader.Value.ToString());
                                    reader.Read();
                                    reader.Read();
                                } while (reader.Name != "tape");
                            } break;
                    }

                }
            }
            reader.Close();
            stream.Close();
        }

        public static void writeXML(string path, Settings1 param2)
        {
            FileStream stream = new FileStream(path, FileMode.Create);

            XmlWriterSettings writerSettings = new XmlWriterSettings
            {
                Indent = true,
                IndentChars = "  ",
                NewLineChars = "\r\n",
                NewLineHandling = NewLineHandling.Replace
            };

            XmlWriter writer = XmlWriter.Create(stream, writerSettings);

            writer.WriteStartElement("body");
            if (param2.AlphabetType != "")
            {
                writer.WriteStartElement("AlphabetType");
                writer.WriteString(param2.AlphabetType);
                writer.WriteEndElement();
            }
            if (param2.StateType != "")
            {
                writer.WriteStartElement("StateType");
                writer.WriteString(param2.StateType);
                writer.WriteEndElement();
            }
            if (param2.StartState != "")
            {
                writer.WriteStartElement("StartState");
                writer.WriteString(param2.StartState);
                writer.WriteEndElement();
            }
            if (param2.Carret != "")
            {
                writer.WriteStartElement("carret");
                writer.WriteString(param2.Carret);
                writer.WriteEndElement();
            }
            if (param2.Tape.Count >= 1)
            {
                writer.WriteStartElement("tape");
                foreach (string x in param2.Tape)
                {
                    writer.WriteStartElement("tapeelem");
                    writer.WriteString(x);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }

            foreach (Settings1.Transition i in param2.Transitions)
            {
                writer.WriteStartElement("transition");
                writer.WriteStartElement("state");
                writer.WriteString(i.state.ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("value");
                writer.WriteString(i.value.ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("nextState");
                writer.WriteString(i.nextState.ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("newValue");
                writer.WriteString(i.newValue.ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("direction");
                writer.WriteString(i.direction.ToString());
                writer.WriteEndElement();

                writer.WriteEndElement();

            }

            writer.WriteEndElement();

            writer.Close();
            stream.Close();
        }

        public void updateSett()
        {
            sett.AlphabetType = comboBox1.Text;
            sett.StateType = comboBox2.Text;
            sett.Carret = textBox1.Text;
            sett.StartState = textBox2.Text;
            sett.Tape.Clear();
            string[] temp = richTextBox2.Text.Split('\n');
            foreach (string x in temp)
            {
                //Console.WriteLine("xx" + x + "xx");
                if (x.Length >= 1)
                {
                    sett.Tape.Add(x);
                }
            }
        }

        private void /*readXML*/button4_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = "C:\\Users\\Kamil\\Desktop\\";
            openFileDialog1.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {

                readXML(openFileDialog1.FileName, sett);

                transToShow = 0;
                button2.Enabled = false;
                button3.Enabled = sett.Transitions.Count() >= 2 ? true : false;

                dispSettings();
            }
        }

        private void /* < */button2_Click(object sender, EventArgs e)
        {
            if (transToShow >= 1)
            {
                transToShow--;
                updateSett();
                dispSettings();
                if (transToShow == 0)
                {
                    button2.Enabled = false;
                }
                button3.Enabled = true;
            }
        }

        private void /* > */button3_Click(object sender, EventArgs e)
        {
            if (transToShow < sett.Transitions.Count)
            {
                transToShow++;
                updateSett();
                dispSettings();
                if (transToShow == sett.Transitions.Count-1)
                {
                    button3.Enabled = false;
                }
                button2.Enabled = true;
            }
        }

        private void btAddTrans_Click(object sender, EventArgs e)
        {
            sett += new Tuple<string, string, string, string, string>("-1", "-1", "-1", "-1", "-1");
            if (sett.Transitions.Count >= 2)
            {
                button3.Enabled = true;
            }
            updateSett();
            dispSettings();
        }

        private void btRemTrans_Click(object sender, EventArgs e)
        {
            if (sett.Transitions.Count > 0)
            {
                sett.Transitions.RemoveAt(transToShow);
                transToShow = 0;
                button2.Enabled = false;
                button3.Enabled = sett.Transitions.Count() >= 2 ? true : false;
                updateSett();
                dispSettings();
            }
        }

        private void btEditTrans_Click(object sender, EventArgs e)
        {
            if (btEditTrans.Text == "Edit")
            {
                richTextBox1.Enabled = true;
                btAddTrans.Enabled = false;
                btRemTrans.Enabled = false;
                button2.Enabled = false;
                button3.Enabled = false;
                btEditTrans.Text = "Save";
            }
            else
            {
                sett.editTrans(transToShow, richTextBox1.Text);
                richTextBox1.Enabled = false;
                btAddTrans.Enabled = true;
                btRemTrans.Enabled = true;
                if (transToShow != 0)
                {
                    button2.Enabled = true;
                }
                if (transToShow < sett.Transitions.Count-1)
                {
                    button3.Enabled = true;
                }
                btEditTrans.Text = "Edit";
            }

        }

        private void /*Apply*/button1_Click(object sender, EventArgs e)
        {
            updateSett();
            sett.assemblyPath = "";
            paramSett.duplicateSett(sett);
            //Console.WriteLine("Carret: " + paramSett.Carret);
            //Console.WriteLine("StartState: " + paramSett.StartState);
            Dispose();
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void /*SaveXML*/button5_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.InitialDirectory = "C:\\Users\\Kamil\\Desktop\\";
            saveFileDialog1.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                updateSett();
                writeXML(saveFileDialog1.FileName, sett);
            }
        }

    }
}
